import operator
from collections.abc import Set
from dataclasses import dataclass

import typing as t

V = 12


@dataclass
class FuzzyElement:
    value: float
    affiliation: float

    def __eq__(self, other: 'FuzzyElement') -> bool:
        return self.value == other.value

    def __lt__(self, other: 'FuzzyElement') -> bool:
        return self.value < other.value

    def __gt__(self, other: 'FuzzyElement') -> bool:
        return self.value > other.value

    def __le__(self, other: 'FuzzyElement') -> bool:
        return self.value <= other.value

    def __ge__(self, other: 'FuzzyElement') -> bool:
        return self.value >= other.value

    def __hash__(self):
        return hash(self.value)

    def __repr__(self):
        return f'<{self.value}, {self.affiliation:.2f}>'


class FuzzySet(Set):
    _elements: set[FuzzyElement]

    def __init__(self, data: t.Optional[t.Iterable[FuzzyElement]] = None):
        super(FuzzySet, self).__init__()
        if data is not None:
            self._elements = set(data)
        else:
            self._elements = set()

    def __or__(self, other: 'FuzzySet') -> 'FuzzySet':
        new_set = FuzzySet()
        for el in self:
            for other_el in other:
                if el.value == other_el.value:
                    new_set.add(
                        FuzzyElement(value=el.value, affiliation=min(el.affiliation, other_el.affiliation))
                    )
                    break
        return new_set

    def __and__(self, other: 'FuzzySet') -> 'FuzzySet':
        new_set = FuzzySet()
        for el in self:
            for other_el in other:
                if el.value == other_el.value:
                    new_set.add(
                        FuzzyElement(value=el.value, affiliation=max(el.affiliation, other_el.affiliation))
                    )
                    break
        return new_set

    def __len__(self) -> int:
        return len(self._elements)

    def add(self, value: FuzzyElement):
        self._elements.add(value)

    def remove(self, value: FuzzyElement) -> None:
        self._elements.remove(value)

    def __contains__(self, item: FuzzyElement) -> bool:
        return item in self._elements

    def __iter__(self) -> t.Iterable[FuzzyElement]:
        return iter(self._elements)

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__}: {sorted(self)}>'

    @property
    def bearer(self) -> 'FuzzySet':
        return FuzzySet(el for el in self if el.affiliation > 0)

    @property
    def height(self) -> float:
        return max(self, key=lambda el: el.affiliation).affiliation

    @property
    def mode(self) -> float:
        return max(self, key=lambda el: el.affiliation).value

    @property
    def alpha_slice(self) -> 'FuzzySet':
        return FuzzySet(el for el in self if el.affiliation >= 0.5)

    @property
    def core(self) -> 'FuzzySet':
        return FuzzySet(el for el in self if el.affiliation == 1)

    @property
    def support(self) -> 'FuzzySet':
        return FuzzySet(el for el in self if el.affiliation > 0)

    @property
    def complement(self) -> 'FuzzySet':
        return FuzzySet(FuzzyElement(value=el.value, affiliation=1 - el.affiliation) for el in self)


class FuzzyNumber(FuzzySet):
    def _fuzzy_arithmetic(self, arithmetic_function: t.Callable, second_number: 'FuzzyNumber') -> 'FuzzyNumber':
        tmp_elems_map = {}
        for fe_1 in self:
            for fe_2 in second_number:
                res_el = FuzzyElement(
                    value=arithmetic_function(fe_1.value, fe_2.value),
                    affiliation=min(fe_1.affiliation, fe_2.affiliation)
                )
                if curr_value := tmp_elems_map.get(res_el.value, None):
                    tmp_elems_map[res_el.value] = res_el if curr_value.affiliation < res_el.affiliation else curr_value
                else:
                    tmp_elems_map[res_el.value] = res_el

        return FuzzyNumber(tmp_elems_map.values())

    def __add__(self, other: 'FuzzyNumber') -> 'FuzzyNumber':
        return self._fuzzy_arithmetic(operator.add, other)

    def __mul__(self, other: 'FuzzyNumber') -> 'FuzzyNumber':
        return self._fuzzy_arithmetic(operator.mul, other)

    def __sub__(self, other: 'FuzzyNumber') -> 'FuzzyNumber':
        return self._fuzzy_arithmetic(operator.sub, other)

    def __truediv__(self, other: 'FuzzyNumber') -> 'FuzzyNumber':
        return self._fuzzy_arithmetic(operator.truediv, other)
