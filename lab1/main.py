from lab1.base import FuzzySet, FuzzyElement, V


def main():
    print('------Generating sets--------------')
    set_a = FuzzySet(FuzzyElement(float(i), i / V) for i in range(0, V + 1))
    set_b = FuzzySet(FuzzyElement(i * 0.5, i / (2 * V)) for i in range(0, V + 1))
    print(f'Set A: {set_a}')
    print(f'Set B: {set_b}')
    print('------Calculating bearer-----------')
    print(f'A bearer: {set_a.bearer}')
    print(f'B bearer: {set_b.bearer}')
    print('------Calculating height-----------')
    print(f'A height = {set_a.height}')
    print(f'B height = {set_b.height}')
    print('------Calculating mode-------------')
    print(f'A mode = {set_a.mode}')
    print(f'B mode = {set_b.mode}')
    print('------Calculating alpha_slice------')
    print(f'A bearer: {set_a.alpha_slice}')
    print(f'B bearer: {set_b.alpha_slice}')
    print('------Calculating core-------------')
    print(f'A height = {set_a.core}')
    print(f'B height = {set_b.core}')
    print('------Calculating support----------')
    print(f'A mode = {set_a.support}')
    print(f'B mode = {set_b.support}')
    print('------Calculating intersection between set A and B------')
    print(f'A ∩ B = {set_a | set_b}')
    print('------Calculating union between set A and B-------------')
    print(f'A ∪ B = {set_a & set_b}')
    print('------Calculating complement----------')
    print(f'A complement = {set_a.complement}')
    print(f'B complement = {set_b.complement}')


if __name__ == '__main__':
    main()
